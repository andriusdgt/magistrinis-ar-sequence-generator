package com.jaguarsoft.ritmintatorius.arSequenceGenerator

import java.util.*

class ARSequenceGenerator {

    fun generate(sampleSize: Int, sampleCount: Int, b: Float, mean: Float, ARParams: List<Double>) =
        mutableListOf<MutableList<Float>>().apply {
            for (t in 1..sampleCount)
                this.add(getSingleSequence(sampleSize, b, mean, ARParams))
        }

    private fun getSingleSequence(
        sampleSize: Int,
        b: Float,
        mean: Float,
        ARParams: List<Double>
    ): MutableList<Float> =
        mutableListOf<Float>().let {
            for (i in 0 until MODEL_INITIALIZATION_TIME + sampleSize)
                it.add(x(it, i, b, ARParams))
            for (i in 0 until it.size)
                it[i] += mean
            return it.subList(MODEL_INITIALIZATION_TIME, it.size)
        }

    private fun x(x: List<Float>, i: Int, b: Float, ARParams: List<Double>): Float {
        var xi = b * v()
        for (j in 0 until ARParams.size) {
            xi += ARParams[j] * x.getAt(i - j - 1)
        }
        return xi.toFloat()
    }

    private infix fun List<Float>.getAt(i: Int): Float {
        if (i < 0)
            return 0.0f
        return this[i]
    }

    private fun v() = Random().nextGaussian()

    companion object {
        const val MODEL_INITIALIZATION_TIME = 1000
    }

}