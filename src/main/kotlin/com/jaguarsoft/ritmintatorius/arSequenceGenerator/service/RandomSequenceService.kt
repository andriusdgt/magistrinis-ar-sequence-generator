package com.jaguarsoft.ritmintatorius.arSequenceGenerator.service

import com.jaguarsoft.ritmintatorius.arSequenceGenerator.CsvWriter
import java.lang.System.exit

class RandomSequenceService {

    fun insert(
        sequences: MutableList<MutableList<Float>>,
        insertPosition: Int, intervalEpsilon: Double
    ): MutableList<MutableList<Float>> {

        checkForErrors(insertPosition, sequences)

        for (i in 0 until sequences.size) {
            val toBeInserted1 = (1.toBigDecimal() - intervalEpsilon.toBigDecimal()).toFloat()
            val toBeInserted2 = (1.toBigDecimal() + intervalEpsilon.toBigDecimal()).toFloat()

            sequences[i][insertPosition] = toBeInserted1
            sequences[i][insertPosition + 1] = toBeInserted2
        }

        writeInsertedRandomPairIndicesToCsv(sequences, insertPosition)

        return sequences
    }

    private fun checkForErrors(
        insertPosition: Int,
        sequences: MutableList<MutableList<Float>>
    ) {
        if (insertPosition < 1 || sequences[0].size <= insertPosition) {
            print("Error: Init variables incorrectly initialized")
            exit(-1)
        }
    }

    private fun writeInsertedRandomPairIndicesToCsv(
        sequences: MutableList<MutableList<Float>>,
        insertPosition: Int
    ) {
        val indices = mutableListOf<List<Int>>()
        for (i in 0 until sequences.size)
            indices.add(listOf(insertPosition, insertPosition + 1))
        CsvWriter().write(indices, "out/n=" + sequences[0].size + ",indices.csv")
    }

}