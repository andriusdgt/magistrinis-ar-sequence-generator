package com.jaguarsoft.ritmintatorius.arSequenceGenerator.service

import com.jaguarsoft.ritmintatorius.arSequenceGenerator.Matrix
import com.jaguarsoft.ritmintatorius.arSequenceGenerator.inverse
import com.jaguarsoft.ritmintatorius.arSequenceGenerator.times

class ARParamEstimator {

    val L = 20

    fun calculate(timeSeries: List<Double>): ARParameters {
        val a = getAVector(timeSeries)
        val b = getB(timeSeries, a)
        return ARParameters(a, b)
    }

    private fun getAVector(timeSeries: List<Double>): Matrix {
        return getCMatrix(timeSeries).inverse().times(getCVector(timeSeries))
    }

    private fun getB(timeSeries: List<Double>, A: Matrix): Double {
        var b = 0.0

        for (t in L until timeSeries.size){
            var bComp = 0.0
            for (j in 0 until L)
                bComp += A[j][0] * timeSeries[t - j]
            b += bComp
        }

        return Math.sqrt(b / (timeSeries.size - L))
    }

    private fun getCVector(timeSeries: List<Double>): Matrix {
        val c: Matrix = Array(L) { DoubleArray(L) }
        for (i in 1..L)
            c[i - 1] = doubleArrayOf(getCComponent(timeSeries, i, 0))
        return c
    }

    private fun getCMatrix(timeSeries: List<Double>): Matrix {
        val c: Matrix = Array(L) { DoubleArray(L) }
        for (i in 1..L) {
            val jArray = DoubleArray(L)
            for (j in 1..L)
                jArray[j - 1] = getCComponent(timeSeries, i, j)
            c[i - 1] = jArray
        }
        return c
    }

    private fun getCComponent(timeSeries: List<Double>, i: Int, j: Int): Double {
        var c = 0.0
        for (t in L + 1 until timeSeries.size)
            c += timeSeries[t - i] * timeSeries[t - j]
        return c
    }

    private fun getMean(data: List<Double>) = data.sum() / data.size

}

class ARParameters(val A: Matrix, val b: Double)