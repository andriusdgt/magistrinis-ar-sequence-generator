package com.jaguarsoft.ritmintatorius.arSequenceGenerator

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import java.io.FileWriter

class CsvWriter {

    fun<T> write(outputToWrite: List<List<T>>, filename: String){
        val csvFileFormat = CSVFormat.EXCEL
        val fileWriter = FileWriter(filename)
        val csvFilePrinter = CSVPrinter(fileWriter, csvFileFormat)

        csvFilePrinter.printRecords(outputToWrite)

        fileWriter.flush()
        fileWriter.close()
        csvFilePrinter.close()
    }

}