package com.jaguarsoft.ritmintatorius.arSequenceGenerator

import com.jaguarsoft.ritmintatorius.arSequenceGenerator.service.RandomSequenceService

const val SEQUENCE_NAME = "white"
const val RP_INSERT_POSITION = 20
const val SAMPLE_LENGTH = 100
const val SAMPLE_COUNT = 10000
//const val K = 1

fun main(args: Array<String>) {
    writeSequences()
//    writeSequencesWithRP()
}

private fun writeSequences() {
    for (bIterator in 0..3) {
        CsvWriter().write(generateWhiteSequence(getB(bIterator)), "out/$SEQUENCE_NAME,b=$bIterator.csv")
//        CsvWriter().write(generateA2Sequence(getB(bIterator)), "out/$SEQUENCE_NAME,b=$bIterator.csv")
//        CsvWriter().write(generateA9Sequence(), "out/$SEQUENCE_NAME,b=$bIterator.csv")
    }
}

private fun writeSequencesWithRP() {
    for (bIterator in 0..3)
        for (epsilonIterator in 9 downTo 1) {
            val epsilon = epsilonIterator.toDouble() / 10

            val sequenceToWrite = getInsertedSequence(generateWhiteSequence(getB(bIterator)), epsilon)
//            val sequenceToWrite = getInsertedSequence(generateA2Sequence(getB(bIterator)), epsilon)
//            val sequenceToWrite = getInsertedSequence(generateA9Sequence(), epsilon)

            CsvWriter().write(sequenceToWrite, "out/$SEQUENCE_NAME,b=$bIterator,RPins=0$epsilonIterator.csv")
        }
}

private fun getB(bIterator: Int): Float {
    var b = 0.1f
    if (bIterator == 1)
        b = 0.5f
    if (bIterator == 2)
        b = 1f
    if (bIterator == 3)
        b = 2f
    return b
}

private fun getInsertedSequence(
    sequenceToWrite: MutableList<MutableList<Float>>,
    epsilon: Double
): MutableList<MutableList<Float>> = RandomSequenceService().insert(sequenceToWrite, RP_INSERT_POSITION, epsilon)

private fun generateWhiteSequence(b: Float): MutableList<MutableList<Float>> =
    ARSequenceGenerator().generate(SAMPLE_LENGTH, SAMPLE_COUNT, b, 1.0f, arrayListOf())

private fun generateA2Sequence(b: Float) =
    ARSequenceGenerator().generate(SAMPLE_LENGTH, SAMPLE_COUNT, b, 1.0f, arrayListOf(0.7, -0.5))

private fun generateA9Sequence() =
    ARSequenceGenerator().generate(
        SAMPLE_LENGTH, SAMPLE_COUNT, 0.03263f, 1.02574f,
        arrayListOf(0.66971, -0.030509, 0.08417, 0.28363, 0.03579, -0.24987, -0.014105, -0.046695, 0.16141)
    )
