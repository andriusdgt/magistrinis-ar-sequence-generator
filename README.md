Description
---

This application helps you to generate time series depending on provided autoregressive model

x(i) =  Σ(l=1..L){ a(l)x(i-l) + bV(i)}, i=1..N

L - autoregression equation level

b - noise level

V - Gaussian randomly distributed numbers

It also helps to substitute random pair elements (RP) which can be beneficial for recognition tasks.

Configuring generated sequences parameters 
---

For all related generated sequences configuration refer to source file:
`src/main/kotlin/com/jaguarsoft/ritmintatorius/arSequenceGenerator/Main.kt`

At the top of the class there are four variables, which describe generated sequence parameters.

To generate sequences in usual fashion, `main().writeSequences()` should be left uncommented (only one of the methods can be uncommented).
To generate sequences with substituted RP, `main().writeSequencesWithRP()` should be left uncommented.

Activating this mode will generate 9 sequence files per autoregressive model and noise level "b".
Each file has inserted different degree of elements, influence d by parameter "epsilon", which describes how much inserted element will differ from generated sequence average.
When epsilon is 0 (file accordingly is encoded RPins=00), then inserted RP is equal to sequence average.
When epsilon is 1 (file accordingly is encoded RPins=10), then inserted RP has two elements consisting of 2 times greater and 2 times lesser than average elements.

Sequence generation has 3 default autoregressive model presets implemented:
1. white noise sequence (codenamed "white")
2. sequence according to second level autoregression model (codenamed "A2")
3. sequence according to ninth level autoregression model (codenamed "A9")

To select one of these models, only one corresponding line should be left uncommented (in lines 18-20, 29-32).

By default single run will generate 4 sequence files with different applied background noise level.
You can configure it by commenting those lines (in function `getB()`) and changing loop number to customize number of generated sequences.

Autoregressive models can be configured, added correspondingly at the bottom of the class, next to default presets and invoked during sequence generation.

Running
---

`./gradlew run`

After executing this command, generated sequences in csv files can be found under directory "out/".
Note: "/out" directory must be created for successful file writing.